extends LinkButton

func _on_Start_pressed():
	get_tree().change_scene(str("res://Scenes/Stage1.tscn"))

func _on_Quit_pressed():
	get_tree().quit()

func _on_How2Play_pressed():
	get_tree().change_scene(str("res://Scenes/HowtoPlay.tscn"))

func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
