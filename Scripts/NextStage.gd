extends Area2D

func _on_To_Stage2_body_entered(_body):
	get_tree().change_scene(str("res://Scenes/Stage2.tscn"))

func _on_To_Stage3_body_entered(body):
	get_tree().change_scene(str("res://Scenes/Stage3.tscn"))

func _on_To_Stage4_body_entered(body):
	get_tree().change_scene(str("res://Scenes/FinalStage.tscn"))
