extends KinematicBody2D

export (int) var speed = 100
export (int) var jump_speed = -200
export (int) var GRAVITY = 500
export (int) var double_jump = 0

const UP = Vector2(0, -1)

var velocity = Vector2()
var call = 0
var normal = null
var normal_tile = null
var void_obj = null
var void_tile = null
var night = null
var void_bg = null
var moving_plat = 0

func _ready():
	global.last_position = global_position

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		$AnimatedSprite.play("jump")
		double_jump = 0
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed

func _input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_SPACE and global.time_status():
			if global.hide_time:
				global.hide_time = 0
				get_platform("show")
			else:
				global.hide_time = 1
				get_platform("hide")
		elif event.pressed and event.scancode == KEY_R:
			restart()

func call_platform():
	normal = get_tree().get_current_scene().get_node("Normal")
	normal_tile = get_tree().get_current_scene().get_node("Normal/Platform")
	void_obj = get_tree().get_current_scene().get_node("Void")
	void_tile = get_tree().get_current_scene().get_node("Void/Platform")
	night = get_tree().get_current_scene().get_node("Window/Background/Night")
	void_bg = get_tree().get_current_scene().get_node("Window/Background/Void")

func get_platform(method):
	if call == 0:
		call_platform()
		call = 1
	if method == "show":
		normal.visible = true
		normal_tile.collision_use_parent = false
		
		void_obj.visible = false
		void_tile.collision_use_parent = true
		
		night.visible = true
		void_bg.visible = false
		
		if moving_plat:
			get_tree().get_current_scene().get_node("MovingPlatform").play()
			
	else:
		normal.visible = false
		normal_tile.collision_use_parent = true
		
		void_obj.visible = true
		void_tile.collision_use_parent = false
		
		night.visible = false
		void_bg.visible = true
		
		if moving_plat:
			get_tree().get_current_scene().get_node("MovingPlatform").stop()

func restart():
	if get_tree().get_current_scene().get_name() == "FinalStage":
		get_tree().change_scene(str("res://Scenes/TheEnd.tscn"))
	elif get_tree().get_current_scene().get_name() == "Stage3":
		if global.nocheck:
			get_tree().change_scene(str("res://Scenes/Stage3.tscn"))
		else:
			get_tree().get_current_scene().get_node("MovingPlatform").seek(8)
			get_tree().get_current_scene().get_node("MovingPlatform").play()
	global.death += 1
	global_position = global.last_position
	global.hide_time = 0
	call_deferred("get_platform", "show")

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	if is_on_floor():
		$AnimatedSprite.play("idle")
		velocity += get_floor_velocity()
	get_input()
	velocity = move_and_slide(velocity, UP)

func _on_Spikes_body_entered(body):
	if body.get_name() == "Slime":
		restart()

func _on_Hell_body_entered(body):
	if body.get_name() == "Slime":
		restart()

func _on_Button_body_entered(body):
	moving_plat = 1
