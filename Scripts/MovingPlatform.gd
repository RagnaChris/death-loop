extends Node2D

var activate = 0

func _on_Button_body_entered(body):
	activate = 1
	if global.hide_time == 0:
		play()
	
func play():
	$AnimationPlayer.play("horizontal")

func stop():
	$AnimationPlayer.stop(false)

func seek(time):
	$AnimationPlayer.seek(time, true)
